package mensajes;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Cristian
 */
public class MensajesDAO {
    
    public static void crearMensajeDB(Mensajes mensaje){
        Conexion db_connect = new Conexion();
        
        try(Connection conexion = db_connect.get_connection()) {
            PreparedStatement ps = null;
            
            try {
                String query = "INSERT INTO mensajes (mensaje,"
                        + " autor_mensaje) VALUES (?,?)";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.getAutor_mensaje());
                ps.executeUpdate();
                System.out.println("Mensaje creado");
                
            } catch (SQLException ex) {
                System.out.println("El mensaje no se pudo guardar por: " + ex.getMessage());
            }
            
        } catch (SQLException e) {
            System.out.println("El error es: " + e.getMessage());
        }
    }
    
    public static void leerMensajesDB(){
        Conexion db_connect = new Conexion();
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try(Connection conexion = db_connect.get_connection()) {
            String query = "SELECT * FROM mensajes";
            ps = conexion.prepareStatement(query);
            rs = ps.executeQuery();
            
            while(rs.next()){
                System.out.println("ID: " + rs.getInt("id_mensajes"));
                System.out.println("Mensaje: " + rs.getString("mensaje"));
                System.out.println("Autor: " + rs.getString("autor_mensaje"));
                System.out.println("Fecha: " + rs.getString("fecha_mensaje"));
                System.out.println("");
            }
            
        } catch (SQLException e) {
            System.out.println("No se pudieron recuperar los mensajes");
            System.out.println("Error en el metodo leerMensajesDB en MensajesDAO.java"
                    + " por esto: " + e.getMessage());
        }
    }
    
    public static void borrarMensajesDB(int id_mensajes){
        Conexion db_connect = new Conexion();
        try(Connection conexion = db_connect.get_connection()) {
            PreparedStatement ps = null;
            
            try{
                String query = "DELETE FROM mensajes WHERE id_mensajes = ?";
                ps = conexion.prepareStatement(query);
                ps.setInt(1, id_mensajes);
                ps.executeUpdate();
                System.out.println("El mensaje ha sido borrado");
            } catch (SQLException e) {
                System.out.println("Error de conexion: " + e.getMessage());
                System.out.println("No se pudo borrar el mensaje");
            }
        } catch (SQLException e) {
            System.out.println("Error en el metodo borrarMensajesDB en MensajesDAO.java"
                    + " por esto: " + e.getMessage());
        }
    }
    
    public static void actualizarMensajesDB(Mensajes mensaje){
        Conexion db_connect = new Conexion();
        
        try(Connection conexion = db_connect.get_connection()) {
            PreparedStatement ps = null;
            try{
                String query = "UPDATE mensajes SET mensaje = ? WHERE id_mensajes = ?";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setInt(2, mensaje.getId_mensaje());
                ps.executeUpdate();
                System.out.println("El mensaje se actualizo correctamente");
            } catch (SQLException e) {
                System.out.println("No se pudo actualizar el mensaje por: "
                        + e.getMessage());
                
            }
        }catch (SQLException e) {
            System.out.println("Error en el metodo actualizarMensajesDB en MensajesDAO.java"
                    + " por esto: " + e.getMessage());
        }
        
    }
}
